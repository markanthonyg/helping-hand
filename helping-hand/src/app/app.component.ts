import { Component, ViewEncapsulation, Inject, ViewChild, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  @ViewChild('apts') apts: any;
  @ViewChildren('cleaningServiceDate') cleaningServiceDate: any;

  constructor(private http: HttpClient, public dialog: MatDialog) {
   }

   width = 100;
   height = 100;
   style = {
     position: 'absolute',
     width: '100%',
     height: '100%',
     'z-index': -1
   };
   params = {
     "particles": {
       "number": {
         "value": 6,
         "density": {
           "enable": true,
           "value_area": 800
         }
       },
       "color": {
         "value": "#7ab038"
       },
       "shape": {
         "type": "polygon",
         "stroke": {
           "width": 0,
           "color": "#000"
         },
         "polygon": {
           "nb_sides": 6
         },
         "image": {
           "src": "img/github.svg",
           "width": 100,
           "height": 100
         }
       },
       "opacity": {
         "value": 0.3,
         "random": true,
         "anim": {
           "enable": false,
           "speed": 1,
           "opacity_min": 0.1,
           "sync": false
         }
       },
       "size": {
         "value": 50,
         "random": false,
         "anim": {
           "enable": true,
           "speed": 10,
           "size_min": 40,
           "sync": false
         }
       },
       "line_linked": {
         "enable": false,
         "distance": 200,
         "color": "#ffffff",
         "opacity": 1,
         "width": 2
       },
       "move": {
         "enable": true,
         "speed": 8,
         "direction": "none",
         "random": false,
         "straight": false,
         "out_mode": "out",
         "bounce": false,
         "attract": {
           "enable": false,
           "rotateX": 600,
           "rotateY": 1200
         }
       }
     },
     "interactivity": {
       "detect_on": "canvas",
       "events": {
         "onhover": {
           "enable": false,
           "mode": "grab"
         },
         "onclick": {
           "enable": false,
           "mode": "push"
         },
         "resize": true
       },
       "modes": {
         "grab": {
           "distance": 400,
           "line_linked": {
             "opacity": 1
           }
         },
         "bubble": {
           "distance": 400,
           "size": 40,
           "duration": 2,
           "opacity": 8,
           "speed": 3
         },
         "repulse": {
           "distance": 200,
           "duration": 0.4
         },
         "push": {
           "particles_nb": 4
         },
         "remove": {
           "particles_nb": 2
         }
       }
     },
     "retina_detect": true
   };

  services: any[] = [
    { name: 'Cleaning', icon: '../assets/clean.png' },
    { name: 'Landscaping', icon: '../assets/plant.png' },
    { name: 'Plumbing', icon: '../assets/plunger.png' }
  ]

  aptSizes: any[] = [
    { sqFt: '< 700', price: 100 },
    { sqFt: '700 - 1200', price: 150 },
    { sqFt: '1200 - 1500', price: 200 }
  ]

  houseElements: any[] = [
    { name: 'bedrooms', price: 25, quantity: 0 },
    { name: 'bathrooms', price: 15, quantity: 0 },
    { name: 'living room', price: 20, quantity: 0 },
    { name: 'kitchen', price: 30, quantity: 0 },
    { name: 'floors', price: 10, quantity: 0 }
  ]

  dialogRef;

  currentPage = 'user';

  requests: any = []

  currentService = undefined

  houseElementNotes = ''

  houseCleaningDate;

  landscapeNotes = ''

  landscapeDate;

  plumbingNotes = ''

  plumbingDate;

  getHouseTotal() {
    let total = 0;
    this.houseElements.forEach(e => {
      total += e.price * e.quantity;
    })
    return total;
  }

  backFromServicePage() {
    this.currentService = undefined;
  }

  serviceClicked(srv) {
    this.currentService = srv
  }

  submitPlumbingForm() {
    let body = {
      service: this.currentService.name,
      notes: this.plumbingNotes,
      date: this.plumbingDate
    }

    console.log(body);

    this.http.post('http://helpinghandapi.com/api/submitrequest', body, { withCredentials: false }).subscribe(res => {
      this.dialogRef = this.dialog.open(SubmissionDialog, {
        width: '250px',
        data: body
      });
      this.currentService = undefined;
    });
  }

  submitLandscapeForm() {
    let body = {
      service: this.currentService.name,
      notes: this.landscapeNotes,
      date: this.landscapeDate
    }

    console.log(body);

    this.http.post('http://helpinghandapi.com/api/submitrequest', body, { withCredentials: false }).subscribe(res => {
      this.dialogRef = this.dialog.open(SubmissionDialog, {
        width: '250px',
        data: body
      });
      this.currentService = undefined;
    });
  }

  submitHouseCleaningForm(propertyType) {

    let body = {
      service: this.currentService.name,
      propertyType: propertyType,
      houseElements: this.houseElements,
      date: this.houseCleaningDate,
      notes: this.houseElementNotes,
      total: this.getHouseTotal()
    }

    console.log(body)

    this.http.post('http://helpinghandapi.com/api/submitrequest', body, { withCredentials: false }).subscribe(res => {
      this.dialogRef = this.dialog.open(SubmissionDialog, {
        width: '250px',
        data: body
      });
      this.currentService = undefined;
    });
  }

  submitCleaningForm(propertyType, aptSize) {

    let body = {
      service: this.currentService.name,
      propertyType: propertyType,
      apartmentData: {
        price: aptSize.price,
        sqFt: aptSize.sqFt
      },
      date: aptSize.date,
      notes: aptSize.notes
    }

    console.log(body)

    this.http.post('http://helpinghandapi.com/api/submitrequest', body, { withCredentials: false }).subscribe(res => {
      this.dialogRef = this.dialog.open(SubmissionDialog, {
        width: '250px',
        data: body
      });
      this.currentService = undefined;
    });


  }
}

@Component({
  selector: 'submission-dialog',
  template: `<h1>Thank you!</h1>
    We got your request for:
    <p><b>{{ data.service }}</b></p>
    <p><b>{{ data.description }}</b></p>
    <p>On: {{data.date }}</p>`
})
export class SubmissionDialog {
  constructor(public dialogRef: MatDialogRef<SubmissionDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
    setTimeout(() => {
      this.dialogRef.close();
    }, 20000);
  }
}
