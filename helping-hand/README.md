# HelpingHand

## Install dependencies
Install node.js (https://nodejs.org/en/download/)

## Setup the app
On the command line, run:  
`git clone https://gitlab.com/helping-hand/helping-hand.git`  
`cd helping-hand`  
`npm install`

## Run the app
Execute command from root level in project:  
`npm start`  
Open a browser, in the url type: localhost:4200

## Other stuff

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
