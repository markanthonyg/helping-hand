import { Component } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  width = 100;
  height = 100;
  style = {
    position: 'absolute',
    width: '100%',
    height: '100%',
    'z-index': -1
  };
  params = {
    "particles": {
      "number": {
        "value": 6,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#7ab038"
      },
      "shape": {
        "type": "polygon",
        "stroke": {
          "width": 0,
          "color": "#000"
        },
        "polygon": {
          "nb_sides": 6
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 50,
        "random": false,
        "anim": {
          "enable": true,
          "speed": 10,
          "size_min": 40,
          "sync": false
        }
      },
      "line_linked": {
        "enable": false,
        "distance": 200,
        "color": "#ffffff",
        "opacity": 1,
        "width": 2
      },
      "move": {
        "enable": true,
        "speed": 8,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "grab"
        },
        "onclick": {
          "enable": false,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  };

  calendarPlugins = [dayGridPlugin]; // important!

  requests: any[] = [
    { business: 'Amazon', service: 'Cleaning', propertyType: 'house', date: new Date(), notes: 'clark shit on the carpet' },
    { business: 'Amazon', service: 'Cleaning', propertyType: 'apartment', date: new Date(new Date().setDate(new Date().getDate() - 5)) },
    { business: 'Amazon', service: 'Landscaping', date: new Date(new Date().setDate(new Date().getDate() - 15)) },
    { business: 'Amazon', service: 'Plumbing', date: new Date(new Date().setDate(new Date().getDate() + 2)) },
    { business: 'Amazon', service: 'Plumbing', date: new Date(new Date().setDate(new Date().getDate() + 4)) },
    { business: 'Amazon', service: 'Plumbing', date: new Date(new Date().setDate(new Date().getDate() + 6)) }
  ]

  today = new Date();
  next7Days = this.requests.filter(e => {
    return e.date >= this.today && e.date <= new Date().setDate(this.today.getDate() + 7)
  })

  ngOnInit() {
    console.log(this.next7Days)
  }

}
